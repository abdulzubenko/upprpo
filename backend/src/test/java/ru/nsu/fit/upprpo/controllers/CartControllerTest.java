package ru.nsu.fit.upprpo.controllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import ru.nsu.fit.upprpo.config.UserDetailsImpl;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.CartRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.RoleRepository;
import ru.nsu.fit.upprpo.security.services.UserService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Stream;

class CartControllerTest extends ControllerTestBase {
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private UserService service;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private CartController cartController;

	public HashMap<String, User> users;

	public static User currentUser;

	@BeforeEach
	public void setup() {
		users = new HashMap<>();
		for (int i = 1; i <= 10; i++)
		{
			Product product = new Product();
			product.setName("Porridge_" + i);
			product.setManufacturer("Erokha_" + i);
			product.setId((long) i);
			product.setDescription("Om-nom, porridge");
			product.setPrice(new BigDecimal(12 + i));
			productRepository.save(product);
		}

		Role adminRole = new Role();
		adminRole.setRoleType(RoleType.ROLE_ADMIN);

		Role customerRole = new Role();
		customerRole.setRoleType(RoleType.ROLE_CUSTOMER);

		roleRepository.save(adminRole);
		roleRepository.save(customerRole);

		User admin = new User();
		admin.setRole(adminRole);
		admin.setUsername("admin");
		admin.setPassword(passwordEncoder.encode("admin"));
		service.saveUser(admin);

		users.put("admin", admin);

		User user = new User();
		user.setRole(adminRole);
		user.setUsername("user");
		user.setPassword(passwordEncoder.encode("user"));
		users.put("user", user);

		User bad = new User();
		bad.setUsername("bad");
		bad.setPassword(passwordEncoder.encode("bad"));
		bad.setRole(customerRole);
		users.put("bad", bad);

		service.saveUser(user);

		mock = MockMvcBuilders.standaloneSetup(cartController)
				.setCustomArgumentResolvers(new TestingDetailsResolver()).build();
	}

	static Stream<TestingParameters> getCartTestParams() {
		return Stream.of(
				new TestingParameters(3).setInfo("admin"),
				new TestingParameters(6).setInfo("user"),
				new TestingParameters(-1).setInfo("bad"));
	}

	@ParameterizedTest
	@MethodSource("getCartTestParams")
	void getCartTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);
		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.get(Endpoints.CART)
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	static Stream<TestingParameters> addCartTestParams() {
		return Stream.of(
				new TestingParameters(1).setInfo("admin").addParam("productId", "1"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1"),
				new TestingParameters(-2).setInfo("admin").addParam("productId", "-1"),
				new TestingParameters(-2).setInfo("user").addParam("productId", "-1"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "-1"));
	}

	@ParameterizedTest
	@MethodSource("addCartTestParams")
	void addCartTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);

		Long id = Long.valueOf(parameter.params.get("productId"));
		if (id == 1) {
			id = productRepository.findAll().get(0).getId();
		}

		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.put(Endpoints.CART)
						.param("productId", id.toString())
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		if (parameter.result == -2)
		{
			Assertions.assertEquals(400, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	static Stream<TestingParameters> setQuantityTestParams() {
		return Stream.of(
				new TestingParameters(1).setInfo("admin").addParam("productId", "1").addParam("quantity", "2"),
				new TestingParameters(1).setInfo("admin").addParam("productId", "1").addParam("quantity", "-1"),
				new TestingParameters(-2).setInfo("admin").addParam("productId", "1").addParam("quantity", "kek"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1").addParam("quantity", "-1"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1").addParam("quantity", "2"),
				new TestingParameters(-2).setInfo("user").addParam("productId", "1").addParam("quantity", "kek"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("quantity", "-1"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("quantity", "2"),
				new TestingParameters(-2).setInfo("bad").addParam("productId", "1").addParam("quantity", "kek"),
				new TestingParameters(-2).setInfo("admin").addParam("productId", "-1").addParam("quantity", "2"),
				new TestingParameters(-2).setInfo("user").addParam("productId", "-1").addParam("quantity", "2"));
	}

	@ParameterizedTest
	@MethodSource("setQuantityTestParams")
	void setQuantityTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);

		Long id = Long.valueOf(parameter.params.get("productId"));
		if (id == 1) {
			id = productRepository.findAll().get(0).getId();
			MockHttpServletRequestBuilder requestBuilder =
					MockMvcRequestBuilders
							.put(Endpoints.CART)
							.param("productId", id.toString())
							.contentType(MediaType.APPLICATION_JSON);

			var res = mock.perform(requestBuilder).andReturn();
		}

		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.put(Endpoints.CART_QUANTITY)
						.param("productId", id.toString())
						.param("quantity", parameter.params.get("quantity"))
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		if (parameter.result == -2)
		{
			Assertions.assertEquals(400, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	static Stream<TestingParameters> removeTestParams() {
		return Stream.of(
				new TestingParameters(1).setInfo("admin").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(1).setInfo("admin").addParam("productId", "1").addParam("exist", "false"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1").addParam("exist", "false"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("exist", "false"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(-2).setInfo("user").addParam("productId", "-1").addParam("exist", "false"));
	}

	@ParameterizedTest
	@MethodSource("removeTestParams")
	void removeTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);

		Long id = Long.valueOf(parameter.params.get("productId"));
		if (id == 1) {
			id = productRepository.findAll().get(0).getId();
			if (Boolean.parseBoolean(parameter.params.get("exist"))) {
				MockHttpServletRequestBuilder requestBuilder =
						MockMvcRequestBuilders
								.put(Endpoints.CART)
								.param("productId", id.toString())
								.contentType(MediaType.APPLICATION_JSON);

				var res = mock.perform(requestBuilder).andReturn();
			}
		}

		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.delete(Endpoints.CART)
						.param("productId", id.toString())
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		if (parameter.result == -2)
		{
			Assertions.assertEquals(400, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	static Stream<TestingParameters> clearTestParams() {
		return Stream.of(
				new TestingParameters(1).setInfo("admin").addParam("exist", "true"),
				new TestingParameters(1).setInfo("admin").addParam("exist", "false"),
				new TestingParameters(1).setInfo("user").addParam("exist", "true"),
				new TestingParameters(1).setInfo("user").addParam("exist", "false"),
				new TestingParameters(-1).setInfo("bad").addParam("exist", "true"),
				new TestingParameters(-1).setInfo("bad").addParam("exist", "false"));	}

	@ParameterizedTest
	@MethodSource("clearTestParams")
	void clearTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);

		if (Boolean.parseBoolean(parameter.params.get("exist"))) {
			Long id = productRepository.findAll().get(0).getId();
			MockHttpServletRequestBuilder requestBuilder =
					MockMvcRequestBuilders
							.put(Endpoints.CART)
							.param("productId", id.toString())
							.contentType(MediaType.APPLICATION_JSON);
			var res = mock.perform(requestBuilder).andReturn();
		}

		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.delete(Endpoints.CART + "/clear")
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		if (parameter.result == -2)
		{
			Assertions.assertEquals(400, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	static Stream<TestingParameters> purchaseTestParams() {
		return Stream.of(
				new TestingParameters(1).setInfo("admin").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(1).setInfo("admin").addParam("productId", "1").addParam("exist", "false"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(1).setInfo("user").addParam("productId", "1").addParam("exist", "false"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("exist", "true"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("exist", "false"),
				new TestingParameters(-1).setInfo("bad").addParam("productId", "1").addParam("exist", "true"));
	}

	@ParameterizedTest
	@MethodSource("purchaseTestParams")
	void purchaseTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);

		Long id = Long.valueOf(parameter.params.get("productId"));
		if (id == 1) {
			id = productRepository.findAll().get(0).getId();
			if (Boolean.parseBoolean(parameter.params.get("exist"))) {
				MockHttpServletRequestBuilder requestBuilder =
						MockMvcRequestBuilders
								.put(Endpoints.CART)
								.param("productId", id.toString())
								.contentType(MediaType.APPLICATION_JSON);

				var res = mock.perform(requestBuilder).andReturn();
			}
		}

		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.put(Endpoints.PURCHASE)
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		if (parameter.result == -2)
		{
			Assertions.assertEquals(400, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	static Stream<TestingParameters> getHistoryTestParams() {
		return Stream.of(
				new TestingParameters(1).setInfo("admin").addParam("exist", "true"),
				new TestingParameters(1).setInfo("admin").addParam("exist", "false"),
				new TestingParameters(1).setInfo("user").addParam("exist", "true"),
				new TestingParameters(1).setInfo("user").addParam("exist", "false"),
				new TestingParameters(-1).setInfo("bad").addParam("exist", "true"),
				new TestingParameters(-1).setInfo("bad").addParam("exist", "false"));
	}

	@ParameterizedTest
	@MethodSource("purchaseTestParams")
	void getHistoryTest(TestingParameters parameter) throws Exception {
		currentUser = users.get(parameter.userName);
		if (Boolean.valueOf(parameter.params.get("exist")))
		{
			Long id = productRepository.findAll().get(0).getId();
			{
				MockHttpServletRequestBuilder requestBuilder =
						MockMvcRequestBuilders
								.put(Endpoints.CART)
								.param("productId", id.toString())
								.contentType(MediaType.APPLICATION_JSON);
				var res = mock.perform(requestBuilder).andReturn();
			}
			{
				MockHttpServletRequestBuilder requestBuilder =
						MockMvcRequestBuilders
								.put(Endpoints.PURCHASE)
								.contentType(MediaType.APPLICATION_JSON);

				var res = mock.perform(requestBuilder).andReturn();
			}
		}

		MockHttpServletRequestBuilder requestBuilder =
				MockMvcRequestBuilders
						.get(Endpoints.HISTORY)
						.contentType(MediaType.APPLICATION_JSON);

		var res = mock.perform(requestBuilder).andReturn();

		if (parameter.result == -1) {
			Assertions.assertEquals(401, res.getResponse().getStatus());
			return;
		}
		if (parameter.result == -2)
		{
			Assertions.assertEquals(400, res.getResponse().getStatus());
			return;
		}
		Assertions.assertEquals(200, res.getResponse().getStatus());
	}

	@AfterEach
	public void clean() {
		service.deleteAll();
		cartRepository.deleteAll();
		roleRepository.deleteAll();
		productRepository.deleteAll();
	}

	private static class TestingParameters {
		public String userName;
		public int result = 0;
		public HashMap<String, String> params;
		TestingParameters(int result) {
			this.result = result;
			params = new HashMap<>();
		}
		public TestingParameters setInfo(String name) {
			this.userName = name;
			return this;
		}
		public TestingParameters addParam(String param, String value) {
			this.params.put(param, value);
			return this;
		}

	}

	private static class TestingDetailsResolver implements HandlerMethodArgumentResolver {

		@Override
		public boolean supportsParameter(MethodParameter parameter) {
			return parameter.getParameterType().isAssignableFrom(UserDetailsImpl.class);
		}

		@Override
		public Object resolveArgument(
				MethodParameter parameter,
				ModelAndViewContainer mavContainer,
				NativeWebRequest webRequest,
				WebDataBinderFactory binderFactory) throws Exception {
			return UserDetailsImpl.fromUserEntityToCustomUserDetails(currentUser);
		}

	}
}
