package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponse;
import ru.nsu.fit.upprpo.controllers.responses.ReviewResponse;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.*;
import ru.nsu.fit.upprpo.security.services.UserService;


import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Stream;



class FeedbackControllerRetrieveTest extends ControllerTestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService service;

    public HashMap<String, User> users;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @BeforeEach
    public void setup() {


        Category category = new Category();
        category.setName("Category");
        category.setChildren(new HashSet<>());
        categoryRepository.save(category);

        Product product = new Product();
        product.setName("Product");
        product.setManufacturer("Company");
        product.setId(1L);
        product.setDescription("Description");
        product.setPrice(new BigDecimal(12));
        product.setCategory(category);
        productRepository.save(product);


        users = new HashMap<>();

        Role adminRole = new Role();
        adminRole.setRoleType(RoleType.ROLE_ADMIN);

        Role customerRole = new Role();
        customerRole.setRoleType(RoleType.ROLE_CUSTOMER);

        roleRepository.save(adminRole);
        roleRepository.save(customerRole);

        for (int i = 0; i < 3; ++i) {

            User user = new User();
            user.setRole(customerRole);
            user.setUsername("user" + i);
            user.setPassword(passwordEncoder.encode("user"));
            users.put(user.getUsername(), user);
            service.saveUser(user);
        }

    }

    static Stream<List<Integer>> getRatingsTestParams() {
        return Stream.of(
                List.of(),
                List.of(1, 2, 3, 4, 5),
                List.of(3),
                List.of(5, 5, 5),
                List.of(1, 1, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("getRatingsTestParams")
    void getRatingsTest(List<Integer> ratings) throws Exception {

        long productId = productRepository.findAll().get(0).getId();

        for (int value : ratings) {
            Rating rating = new Rating();
            rating.setProduct(productRepository.findById(productId));
            rating.setValue(value);
            ratingRepository.save(rating);
        }

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.PRODUCT + "/" + productId)
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();

        var resultBody = new Gson().fromJson(res.getResponse().getContentAsString(), ProductResponse.class);

        if (ratings.isEmpty()) {
            assertEquals(0.0, resultBody.getRating());
        } else {
            assertEquals(ratings.stream().reduce(0, Integer::sum) / (double) ratings.size(),
                    resultBody.getRating());
        }
        assertEquals(ratings.size(), resultBody.getRatingNumber());
    }

    static Stream<Arguments> getReviewsTestParams() {
        return Stream.of(

                Arguments.of(
                        Map.of("user0", 3),
                        Map.of("user1", "User1 review")
                ),
                Arguments.of(
                        Map.of("user0", 3, "user1", 4),
                        Map.of("user1", "User1 review", "user2", "User2 review")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("getReviewsTestParams")
    void getReviewsTest(Map<String, Integer> ratings, Map<String, String> reviews) throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        Product product = productRepository.findById(productId);

        for (var entry : ratings.entrySet()) {
            Rating rating = new Rating();
            rating.setAuthor(users.get(entry.getKey()));
            rating.setValue(entry.getValue());
            rating.setProduct(product);
            ratingRepository.save(rating);
        }

        for (var entry : reviews.entrySet()) {
            Review review = new Review();
            review.setText(entry.getValue());
            review.setAuthor(users.get(entry.getKey()));
            review.setProduct(product);
            reviewRepository.save(review);
        }

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.REVIEWS_LIST)
                        .param("productId", String.valueOf(productId))
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();

        System.out.println(res.getResponse().getStatus());

        Type listType = new TypeToken<ArrayList<ReviewResponse>>(){}.getType();
        List<ReviewResponse> resultBody = new Gson().fromJson(res.getResponse().getContentAsString(), listType);

        assertEquals(reviews.size(), resultBody.size());

        for (var review : resultBody) {
            String username = review.getUsername();
            assertEquals(ratings.getOrDefault(username, 0), review.getRating());
            assertEquals(reviews.get(username), review.getText());
        }
    }

    @Test
    void brokenProductReviewTest() throws Exception {

        // non-existent product
        long productId = productRepository.findAll()
                .stream()
                .map(Product::getId)
                .max(Long::compare)
                .map(a -> a + 1)
                .orElse(1L);

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.REVIEWS_LIST)
                        .param("productId", String.valueOf(productId))
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.NOT_FOUND.value(), res.getResponse().getStatus());
    }

    @AfterEach
    void cleanUp() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        ratingRepository.deleteAll();
        reviewRepository.deleteAll();
        roleRepository.deleteAll();
        service.deleteAll();

    }

}
