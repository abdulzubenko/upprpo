package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.AfterEach;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import ru.nsu.fit.upprpo.config.UserDetailsImpl;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.requests.ReviewRequest;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.*;
import ru.nsu.fit.upprpo.security.services.UserService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Stream;

class FeedbackControllerAuthenticatedTest extends ControllerTestBase {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService service;

    public HashMap<String, User> users;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private FeedbackController feedbackController;

    public static User currentUser;

    @BeforeEach
    public void setup() {


        Category category = new Category();
        category.setName("Category");
        category.setChildren(new HashSet<>());
        categoryRepository.save(category);

        Product product = new Product();
        product.setName("Product");
        product.setManufacturer("Company");
        product.setId(1L);
        product.setDescription("Description");
        product.setPrice(new BigDecimal(12));
        product.setCategory(category);
        productRepository.save(product);


        users = new HashMap<>();

        Role adminRole = new Role();
        adminRole.setRoleType(RoleType.ROLE_ADMIN);

        Role customerRole = new Role();
        customerRole.setRoleType(RoleType.ROLE_CUSTOMER);

        roleRepository.save(adminRole);
        roleRepository.save(customerRole);

        for (int i = 0; i < 3; ++i) {

            User user = new User();
            user.setRole(customerRole);
            user.setUsername("user" + i);
            user.setPassword(passwordEncoder.encode("user"));
            users.put(user.getUsername(), user);
            service.saveUser(user);
        }
        mock = MockMvcBuilders.standaloneSetup(feedbackController)
                .setCustomArgumentResolvers(new TestingDetailsResolver()).build();
    }

    private static class TestingDetailsResolver implements HandlerMethodArgumentResolver {

        @Override
        public boolean supportsParameter(MethodParameter parameter) {
            return parameter.getParameterType().isAssignableFrom(UserDetailsImpl.class);
        }

        @Override
        public Object resolveArgument(
                MethodParameter parameter,
                ModelAndViewContainer mavContainer,
                NativeWebRequest webRequest,
                WebDataBinderFactory binderFactory) throws Exception {
            return UserDetailsImpl.fromUserEntityToCustomUserDetails(currentUser);
        }

    }

    @AfterEach
    void cleanUp() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        ratingRepository.deleteAll();
        reviewRepository.deleteAll();
        roleRepository.deleteAll();
        service.deleteAll();

    }

    static Stream<Integer> submitRatingTestParams() {
        return Stream.of(-1, 0, 1, 2, 3, 4, 5, 6, Integer.MAX_VALUE, Integer.MIN_VALUE);
    }

    @ParameterizedTest
    @MethodSource("submitRatingTestParams")
    void submitRatingTest(int rating) throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        Product product = productRepository.findById(productId);
        currentUser = users.get("user0");
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .put(Endpoints.RATING)
                        .param("productId", String.valueOf(productId))
                        .param("rating", String.valueOf(rating))
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();

        if (rating < 1 || rating > 5) {
            assertEquals(HttpStatus.BAD_REQUEST.value(), res.getResponse().getStatus());
            assertTrue(Lists.newArrayList(ratingRepository.findAll()).isEmpty());
        } else {
            assertEquals(HttpStatus.OK.value(), res.getResponse().getStatus());
            assertEquals(1, Lists.newArrayList(ratingRepository.findAll()).size());
            assertEquals(rating, ratingRepository.findByAuthorAndProduct(currentUser, product).orElseThrow().getValue());
        }
    }

    @Test
    void submitReviewTest() throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        Product product = productRepository.findById(productId);
        currentUser = users.get("user0");
        String text = "10 out of 10!";

        ReviewRequest requestBody = new ReviewRequest();
        requestBody.setText(text);
        requestBody.setProductId(productId);
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .post(Endpoints.REVIEW)
                        .content(new Gson().toJson(requestBody))
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();


        assertEquals(HttpStatus.OK.value(), res.getResponse().getStatus());
        assertEquals(1, Lists.newArrayList(reviewRepository.findAll()).size());
        assertEquals(text, reviewRepository.findByProductAndAuthor(product, currentUser).orElseThrow().getText());

    }

    @Test
    void badProductFeedbackTest() throws Exception {
        // non-existent product
        long productId = productRepository.findAll()
                .stream()
                .map(Product::getId)
                .max(Long::compare)
                .map(a -> a + 1)
                .orElse(1L);
        currentUser = users.get("user0");
        {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .put(Endpoints.RATING)
                            .param("productId", String.valueOf(productId))
                            .param("rating", String.valueOf(5))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();


            assertEquals(HttpStatus.BAD_REQUEST.value(), res.getResponse().getStatus());
            assertTrue(Lists.newArrayList(ratingRepository.findAll()).isEmpty());
        }
        {
            ReviewRequest requestBody = new ReviewRequest();
            requestBody.setText("text");
            requestBody.setProductId(productId);
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .post(Endpoints.REVIEW)
                            .content(new Gson().toJson(requestBody))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();


            assertEquals(HttpStatus.BAD_REQUEST.value(), res.getResponse().getStatus());
            assertTrue(Lists.newArrayList(reviewRepository.findAll()).isEmpty());
        }
        {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .get(Endpoints.RATING)
                            .param("productId", String.valueOf(productId))
                            .contentType(MediaType.APPLICATION_JSON);
            var res = mock.perform(requestBuilder).andReturn();

            assertEquals(HttpStatus.NOT_FOUND.value(), res.getResponse().getStatus());
        }
    }

    @Test
    void repeatedReviewTest() throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        Product product = productRepository.findById(productId);
        currentUser = users.get("user0");
        for (int i = 0; i < 5; ++i) {
            String text = String.valueOf(i);

            ReviewRequest requestBody = new ReviewRequest();
            requestBody.setText(text);
            requestBody.setProductId(productId);
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .post(Endpoints.REVIEW)
                            .content(new Gson().toJson(requestBody))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();

            assertEquals(HttpStatus.OK.value(), res.getResponse().getStatus());
            assertEquals(1, Lists.newArrayList(reviewRepository.findAll()).size());
            assertEquals(String.valueOf(i), reviewRepository.findByProductAndAuthor(product, currentUser).orElseThrow().getText());
        }
    }

    @Test
    void repeatedRatingTest() throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        Product product = productRepository.findById(productId);
        currentUser = users.get("user0");
        for (int i = 1; i <= 5; ++i) {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .put(Endpoints.RATING)
                            .param("productId", String.valueOf(productId))
                            .param("rating", String.valueOf(i))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();


            assertEquals(HttpStatus.OK.value(), res.getResponse().getStatus());
            assertEquals(1, Lists.newArrayList(ratingRepository.findAll()).size());
            assertEquals(i, ratingRepository.findByAuthorAndProduct(currentUser, product).orElseThrow().getValue());

        }
    }

    static Stream<Map<String, Integer>> getUserRatingTestParams() {
        return Stream.of(
                Map.of("user0", 1),
                Map.of("user0", 2, "user1", 3),
                Map.of("user1", 4, "user2", 5),
                Map.of()
        );
    }

    @ParameterizedTest
    @MethodSource("getUserRatingTestParams")
    void getUserRatingTest(Map<String, Integer> ratingMap) throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        Product product = productRepository.findById(productId);
        currentUser = users.get("user0");
        for (var username : ratingMap.keySet()) {
            User user = users.get(username);
            Rating rating = new Rating();
            rating.setProduct(product);
            rating.setAuthor(user);
            rating.setValue(ratingMap.get(username));
            ratingRepository.save(rating);
        }

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.RATING)
                        .param("productId", String.valueOf(productId))
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();

        int storedRating = Integer.parseInt(res.getResponse().getContentAsString());

        assertEquals(ratingMap.getOrDefault(currentUser.getUsername(), 0), storedRating);
    }
}
