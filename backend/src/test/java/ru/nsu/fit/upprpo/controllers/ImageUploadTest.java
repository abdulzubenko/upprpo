package ru.nsu.fit.upprpo.controllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import ru.nsu.fit.upprpo.config.UserDetailsImpl;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ImageRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.RoleRepository;
import ru.nsu.fit.upprpo.security.services.UserService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ImageUploadTest extends ControllerTestBase {

    @Autowired
    private ImageController imageController;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService service;

    @Autowired
    private WebApplicationContext webApplicationContext;

    public HashMap<String, User> users;


    public static User currentUser;

    @BeforeEach
    public void setup() {

        Category category = new Category();
        category.setName("Category");
        category.setChildren(new HashSet<>());
        categoryRepository.save(category);

        Product product = new Product();
        product.setName("Product");
        product.setManufacturer("Company");
        product.setId(1L);
        product.setDescription("Description");
        product.setPrice(new BigDecimal(12));
        product.setCategory(category);
        productRepository.save(product);


        users = new HashMap<>();

        Role adminRole = new Role();
        adminRole.setRoleType(RoleType.ROLE_ADMIN);

        Role customerRole = new Role();
        customerRole.setRoleType(RoleType.ROLE_CUSTOMER);

        roleRepository.save(adminRole);
        roleRepository.save(customerRole);


        User admin = new User();
        admin.setRole(adminRole);
        admin.setUsername("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setWishlist(new HashSet<>());
        users.put(admin.getUsername(), admin);
        service.saveUser(admin);

        mock = MockMvcBuilders.standaloneSetup(imageController)
                .setCustomArgumentResolvers(new TestingDetailsResolver()).build();
    }


    private static class TestingDetailsResolver implements HandlerMethodArgumentResolver {

        @Override
        public boolean supportsParameter(MethodParameter parameter) {
            return parameter.getParameterType().isAssignableFrom(UserDetailsImpl.class);
        }

        @Override
        public Object resolveArgument(
                MethodParameter parameter,
                ModelAndViewContainer mavContainer,
                NativeWebRequest webRequest,
                WebDataBinderFactory binderFactory) throws Exception {
            return UserDetailsImpl.fromUserEntityToCustomUserDetails(currentUser);
        }

    }

    @AfterEach
    void cleanUp() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        roleRepository.deleteAll();
        imageRepository.deleteAll();
        service.deleteAll();
    }

    @Test
    void uploadTest() throws Exception {
        currentUser = users.get("admin");

        Product product = productRepository.findAll().get(0);

        MockMultipartFile file = new MockMultipartFile(
                "multipartImage",
                "image.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                getClass().getResourceAsStream("image.jpg"));


        var requestBuilder = MockMvcRequestBuilders
                .multipart(Endpoints.UPLOAD_IMAGE)
                .file(file)
                .param("productId", String.valueOf(product.getId()));

        var result = mock.perform(requestBuilder)
                .andExpect(status().is(200))
                .andReturn();

        assertNotNull(imageRepository.findById(Long.parseLong(result.getResponse().getContentAsString())));
    }


    @Test
    void badUploadTest() throws Exception {
        currentUser = users.get("admin");

        long productId = productRepository.findAll().stream().map(Product::getId).max(Long::compareTo).orElse(0L) + 1;

        MockMultipartFile file = new MockMultipartFile(
                "multipartImage",
                "image.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                getClass().getResourceAsStream("image.jpg"));


        var requestBuilder = MockMvcRequestBuilders
                .multipart(Endpoints.UPLOAD_IMAGE)
                .file(file)
                .param("productId", String.valueOf(productId));

        mock.perform(requestBuilder)
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

    }
}
