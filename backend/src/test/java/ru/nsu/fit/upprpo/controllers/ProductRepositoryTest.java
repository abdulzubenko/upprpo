package ru.nsu.fit.upprpo.controllers;

import org.junit.jupiter.api.*;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepositoryConnector;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

class ProductRepositoryTest extends ControllerTestBase{

	@Resource
	private ProductRepository repository;

	private ProductRepositoryConnector productConnector;


	@BeforeEach
	public void setUp() {
		for (int i = 1; i <= 10; i++)
		{
			Product product = new Product();
			product.setName("Porridge_" + i);
			product.setManufacturer("Erokha_" + i);
			product.setId((long) i);
			product.setDescription("Om-nom, porridge");
			product.setPrice(new BigDecimal(12 + i));
			repository.save(product);
		}
		productConnector = new ProductRepositoryConnector(repository);
	}

	@Test
	@Disabled("Tests runs in random order, so catch true range of ids of generated products is impossible")
	void testFindByIndex() {

		Product product = repository.findById(1L);
		Assertions.assertEquals("Porridge_1", product.getName());
		product = repository.findById(12L);
		Assertions.assertNull(product);
	}


	@Test
	void testSearch() {

		List<Product> products;

		/** Check that not applying filters returns full table. */
		products = productConnector.search(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
		Assertions.assertEquals(10, products.size());

		/** Check that applying name filter works. */
		products = productConnector.search(Optional.of("%P%e%"), Optional.empty(), Optional.empty(), Optional.empty());
		Assertions.assertEquals(10, products.size());

		products = productConnector.search(Optional.of("%1%"), Optional.empty(), Optional.empty(), Optional.empty());
		Assertions.assertEquals(2, products.size());


		/** Check that applying price range filter works. */
		products = productConnector.search(Optional.empty(), Optional.of(BigDecimal.valueOf(15)), Optional.empty(), Optional.empty());
		Assertions.assertEquals(8, products.size());
		for (var t : products) {
			Assertions.assertTrue(BigDecimal.valueOf(15).compareTo(t.getPrice()) <= 0);
		}

		products = productConnector.search(Optional.empty(), Optional.empty(), Optional.of(BigDecimal.valueOf(19)), Optional.empty());
		Assertions.assertEquals(7, products.size());
		for (var t : products) {
			Assertions.assertTrue(BigDecimal.valueOf(19).compareTo(t.getPrice()) >= 0);
		}

		products = productConnector.search(Optional.empty(), Optional.of(BigDecimal.valueOf(15)), Optional.of(BigDecimal.valueOf(19)), Optional.empty());
		Assertions.assertEquals(5, products.size());
		for (var t : products) {
			Assertions.assertTrue(BigDecimal.valueOf(19).compareTo(t.getPrice()) >= 0 && BigDecimal.valueOf(15).compareTo(t.getPrice()) <= 0);
		}

		/** Check that applying manufacturer filter works. */
		products = productConnector.search(Optional.empty(), Optional.empty(), Optional.empty(), Optional.of("%E%H%"));
		Assertions.assertEquals(10, products.size());

		products = productConnector.search( Optional.empty(), Optional.empty(), Optional.empty(), Optional.of("%1%"));
		Assertions.assertEquals(2, products.size());

	}

	@Test
	void testFindAll() {
		Iterable<Product> productIterator = repository.findAll();

		long size = StreamSupport.stream(productIterator.spliterator(), false).count();

		Assertions.assertEquals(10L, size);
	}

	@Test
	void testFindByName() {
		Iterable<Product> productIterator = repository.findByName("Porridge_4");

		long size = StreamSupport.stream(productIterator.spliterator(), false).count();
		Assertions.assertEquals(1L, size);

		Assertions.assertEquals("Erokha_4", productIterator.iterator().next().getManufacturer());
	}

	@AfterEach
	public void cleanUp() {
		repository.deleteAll();
	}
}
