package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import ru.nsu.fit.upprpo.Application;

import javax.transaction.Transactional;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = {Application.class, TestSecurityConfig.class}
)
@ActiveProfiles("test")
@Transactional
public abstract class ControllerTestBase {

	@Autowired
	protected MockMvc mock;
}

@TestConfiguration
@Order(1)
class TestSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception
	{
		// Disable CSRF
		httpSecurity.csrf().disable()
				// Permit all requests without authentication
				.authorizeRequests().anyRequest().permitAll();
	}

}
