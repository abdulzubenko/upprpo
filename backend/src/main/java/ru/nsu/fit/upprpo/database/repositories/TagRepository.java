package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.Tag;

public interface TagRepository extends CrudRepository<Tag, Long> {
	Tag findById(long id);
}
