package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.responses.CartResponse;
import ru.nsu.fit.upprpo.controllers.responses.CartResponseConverter;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponseConverter;
import ru.nsu.fit.upprpo.database.entities.Cart;
import ru.nsu.fit.upprpo.database.entities.CartEntry;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.entities.User;
import ru.nsu.fit.upprpo.database.repositories.CartEntryRepository;
import ru.nsu.fit.upprpo.database.repositories.CartRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.UserRepository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Transactional
public class CartController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private ProductResponseConverter productResponseConverter;

    @Autowired
    private CartResponseConverter cartResponseConverter;

    @GetMapping(Endpoints.CART)
    public ResponseEntity<CartResponse> getCart(@AuthenticationPrincipal UserDetails userDetails) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(cartResponseConverter.mapToCartResponse(user.getCart()));
    }

    @PutMapping(Endpoints.CART)
    public ResponseEntity<CartResponse> addToCart(@AuthenticationPrincipal UserDetails userDetails, @RequestParam Long productId) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId).orElse(null);

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        if (user.getCart().getItems().stream().anyMatch(item -> item.getProduct().getId().equals(productId))) {
            return ResponseEntity.ok(cartResponseConverter.mapToCartResponse(user.getCart()));
        }

        CartEntry entry = new CartEntry();
        entry.setQuantity(1);
        entry.setProduct(product);

        cartEntryRepository.save(entry);

        user.getCart().getItems().add(entry);

        cartRepository.save(user.getCart());
        return ResponseEntity.ok(cartResponseConverter.mapToCartResponse(user.getCart()));
    }

    @PutMapping(Endpoints.CART_QUANTITY)
    public ResponseEntity<CartResponse> setQuantity(@AuthenticationPrincipal UserDetails userDetails,
                                              @RequestParam Long productId, @RequestParam Integer quantity) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId).orElse(null);

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Cart cart = user.getCart();

        CartEntry entry = cart.getItems()
                .stream()
                .filter(e -> e.getProduct().equals(product))
                .findAny()
                .orElse(null);

        if (entry == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        entry.setQuantity(quantity);
        cartEntryRepository.save(entry);
        return ResponseEntity.ok(cartResponseConverter.mapToCartResponse(user.getCart()));
    }

    @DeleteMapping(Endpoints.CART)
    public ResponseEntity<CartResponse> removeFromCart(@AuthenticationPrincipal UserDetails userDetails, @RequestParam Long productId) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId).orElse(null);

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Cart cart = user.getCart();

        Set<CartEntry> removed = cart.getItems()
                .stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .collect(Collectors.toSet());

        cart.getItems().removeAll(removed);
        cartRepository.save(cart);
        cartEntryRepository.deleteAll(removed);

        return ResponseEntity.ok(cartResponseConverter.mapToCartResponse(cart));
    }

    @DeleteMapping(Endpoints.CART + "/clear")
    public ResponseEntity<CartResponse> clearCart(@AuthenticationPrincipal UserDetails userDetails) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Cart cart = user.getCart();

        Set<CartEntry> removed = new HashSet<>(cart.getItems());

        cart.getItems().clear();

        cartEntryRepository.deleteAll(removed);

        return ResponseEntity.ok(cartResponseConverter.mapToCartResponse(cart));
    }

    @PutMapping(Endpoints.PURCHASE)
    public ResponseEntity<List<CartResponse>> purchase(@AuthenticationPrincipal UserDetails userDetails) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Cart cart = user.getCart();

        if (cart.getItems().isEmpty()) {
            return ResponseEntity.ok(user.getHistory()
                    .stream()
                    .sorted(Comparator.comparing(Cart::getTimestamp))
                    .map(cartResponseConverter::mapToCartResponse)
                    .collect(Collectors.toList()));
        }

        cart.setTimestamp(new Timestamp(System.currentTimeMillis()));
        cartRepository.save(cart);

        user.getHistory().add(cart);
        user.setCart(new Cart());

        cartRepository.save(user.getCart());

        userRepository.save(user);
        return ResponseEntity.ok(user.getHistory()
                .stream()
                .sorted(Comparator.comparing(Cart::getTimestamp))
                .map(cartResponseConverter::mapToCartResponse)
                .collect(Collectors.toList()));
    }

    @GetMapping(Endpoints.HISTORY)
    public ResponseEntity<List<CartResponse>> getHistory(@AuthenticationPrincipal UserDetails userDetails) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(user.getHistory()
                .stream()
                .sorted(Comparator.comparing(Cart::getTimestamp))
                .map(cartResponseConverter::mapToCartResponse)
                .collect(Collectors.toList()));
    }

}
