package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.requests.ReviewRequest;
import ru.nsu.fit.upprpo.controllers.responses.ReviewResponse;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.entities.Rating;
import ru.nsu.fit.upprpo.database.entities.Review;
import ru.nsu.fit.upprpo.database.entities.User;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.RatingRepository;
import ru.nsu.fit.upprpo.database.repositories.ReviewRepository;
import ru.nsu.fit.upprpo.database.repositories.UserRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FeedbackController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @PostMapping(path = Endpoints.REVIEW)
    public ResponseEntity<String> postReview(@AuthenticationPrincipal UserDetails userDetails,
                                             @RequestBody ReviewRequest reviewRequest) {

        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(reviewRequest.getProductId());

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Review review = reviewRepository.findByProductAndAuthor(product, user).orElse(new Review());
        review.setAuthor(user);
        review.setText(reviewRequest.getText());
        review.setProduct(product);

        reviewRepository.save(review);

        return ResponseEntity.ok("OK");
    }

    @PutMapping(path = Endpoints.RATING)
    public ResponseEntity<String> postRating(@AuthenticationPrincipal UserDetails userDetails,
                                             @RequestParam long productId,
                                             @RequestParam int rating) {

        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId);

        if (product == null || rating < 1 || rating > 5) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Rating ratingEntity = ratingRepository.findByAuthorAndProduct(user, product).orElse(new Rating());
        ratingEntity.setValue(rating);
        ratingEntity.setAuthor(user);
        ratingEntity.setProduct(product);
        ratingRepository.save(ratingEntity);

        return ResponseEntity.ok("OK");
    }

    @GetMapping(path = Endpoints.REVIEWS_LIST)
    public ResponseEntity<List<ReviewResponse>> getReviews(@RequestParam long productId) {

        Product product = productRepository.findById(productId);
        if (product == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        var reviews = reviewRepository.findByProduct(product)
                .stream()
                .map(review -> new ReviewResponse(review, ratingRepository
                        .findByAuthorAndProduct(review.getAuthor(), product)
                        .map(Rating::getValue)
                        .orElse(0)))
                .sorted(Comparator.comparing(ReviewResponse::getTimestamp))
                .collect(Collectors.toList());

        return ResponseEntity.ok(reviews);
    }

    @GetMapping(Endpoints.RATING)
    public ResponseEntity<Integer> getUserRating(@AuthenticationPrincipal UserDetails userDetails,
                                                 @RequestParam long productId) {

        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId);
        if (product == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok(ratingRepository.findByAuthorAndProduct(user, product).map(Rating::getValue).orElse(0));
    }


}
