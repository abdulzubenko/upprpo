package ru.nsu.fit.upprpo.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.nsu.fit.upprpo.config.UserDetailsImpl;
import ru.nsu.fit.upprpo.database.entities.User;

@Service
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.findByLogin(s);
        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + s);
        }
        return UserDetailsImpl.fromUserEntityToCustomUserDetails(user);
    }


}
