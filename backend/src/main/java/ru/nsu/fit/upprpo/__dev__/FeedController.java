package ru.nsu.fit.upprpo.__dev__;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static ru.nsu.fit.upprpo.database.entities.RoleType.ROLE_ADMIN;
import static ru.nsu.fit.upprpo.database.entities.RoleType.ROLE_CUSTOMER;

@Profile("!test")
@Component
public class FeedController implements ApplicationRunner {

    private static final Random random = new Random(7);

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ImageRepository imageRepository;



    private BigDecimal getNextRandomPrice() {
        BigDecimal bigDecimal = BigDecimal.valueOf(random.nextDouble() * 100);
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        return bigDecimal;
    }

    private void generateCategories(int l1, int l2, int l3, Collection<Category> categories) {
        Set<Category> categories1 = new HashSet<>(categories);
        for (int i = 1; i <= l1; i++) {
            Category category1 = new Category();
            category1.setName(String.format("Gen category %d", i));
            categoryRepository.save(category1);
            categories1.add(category1);

            Set<Category> categories2 = new HashSet<>();
            for (int j = 1; j <= l2; j++) {
                Category category2 = new Category();
                category2.setName(String.format("Gen category %d-%d", i, j));
                categoryRepository.save(category2);
                categories2.add(category2);

                Set<Category> categories3 = new HashSet<>();
                for (int k = 1; k <= l3; k++) {
                    Category category3 = new Category();
                    category3.setName(String.format("Gen category %d-%d-%d", i, j, k));
                    categories3.add(category3);

                    categoryRepository.save(category3);
                }

                category2.setChildren(categories3);
                categoryRepository.save(category2);
            }

            category1.setChildren(categories2);
            categoryRepository.save(category1);
        }

        Category root = new Category();
        root.setName("root");
        root.setChildren(categories1);
        categoryRepository.save(root);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (CollectionUtils.isEmpty(categoryRepository.findAll())) {
            Role admin = new Role();
            admin.setRoleType(ROLE_ADMIN);

            Role customer = new Role();
            customer.setRoleType(ROLE_CUSTOMER);

            roleRepository.saveAndFlush(admin);
            roleRepository.saveAndFlush(customer);


            Cart cart = new Cart();
            cartRepository.save(cart);


            User user = new User();
            user.setRole(admin);
            user.setUsername("admin");
            user.setPassword(passwordEncoder.encode("admin"));

            user.setCart(cart);


            userRepository.save(user);


            Category firstCategory = new Category();
            firstCategory.setName("Category 1");

            Category secondCategory = new Category();
            secondCategory.setName("Category 2");

            Category thirdCategory = new Category();
            thirdCategory.setName("Category 3");



            firstCategory.setChildren(Set.of(secondCategory, thirdCategory));

            categoryRepository.save(secondCategory);
            categoryRepository.save(thirdCategory);
            categoryRepository.save(firstCategory);


            for (int i = 1; i <= 5; i++) {
                Product product = new Product();
                product.setName("Apple barrel " + i);
                product.setManufacturer("Horns & Hooves");
                product.setPrice(getNextRandomPrice());
                product.setDescription("Load oranges in barrels Karamazov brothers " + i);
                product.setCategory(secondCategory);

                productRepository.save(product);

                var file = Objects.requireNonNull(getClass().getResourceAsStream("image2.jpg")).readAllBytes();

                Image image = new Image();
                image.setContent(file);
                image.setProduct(product);
                imageRepository.save(image);
            }

            for (int i = 1; i <= 5; i++) {
                Product product = new Product();
                product.setName("Orange barrel " + i);
                product.setManufacturer("Horns & Hooves");
                product.setPrice(getNextRandomPrice());
                product.setDescription("Load oranges in barrels Karamazov brothers " + i);
                product.setCategory(firstCategory);

                productRepository.save(product);

                var file = Objects.requireNonNull(getClass().getResourceAsStream("image3.jpg")).readAllBytes();

                Image image = new Image();
                image.setContent(file);
                image.setProduct(product);
                imageRepository.save(image);
            }

            for (int i = 1; i <= 5; i++) {
                Product product = new Product();
                product.setName("Lemon barrel " + i);
                product.setManufacturer("Horns & Hooves");
                product.setPrice(getNextRandomPrice());
                product.setDescription("Load oranges in barrels Karamazov brothers " + i);
                product.setCategory(thirdCategory);

                productRepository.save(product);

                var file = Objects.requireNonNull(getClass().getResourceAsStream("image.jpg")).readAllBytes();

                Image image = new Image();
                image.setContent(file);
                image.setProduct(product);
                imageRepository.save(image);
            }

            generateCategories(5, 4, 3, List.of(firstCategory));
        }
    }
}
