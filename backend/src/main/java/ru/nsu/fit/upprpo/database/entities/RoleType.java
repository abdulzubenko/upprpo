package ru.nsu.fit.upprpo.database.entities;

public enum RoleType {
    ROLE_ADMIN, ROLE_CUSTOMER
}
