package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.CartEntry;

public interface CartEntryRepository extends CrudRepository<CartEntry, Long> {

}
