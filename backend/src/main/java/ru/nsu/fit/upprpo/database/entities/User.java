package ru.nsu.fit.upprpo.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Class for db user entity.
 */
@Entity
@Getter @Setter @NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    @ManyToOne
    private Role role;

    @OneToOne
    private Cart cart;

    @OneToMany
    private Set<Cart> history;

    @ManyToMany
    private Set<Product> wishlist;

}
