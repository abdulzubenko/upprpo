package ru.nsu.fit.upprpo.controllers.responses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.fit.upprpo.database.entities.Cart;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;

import java.math.BigDecimal;
import java.util.stream.Collectors;

@Component
public class CartResponseConverter {
    @Autowired
    private ProductResponseConverter productResponseConverter;

    @Autowired
    private ProductRepository productRepository;

    public CartResponse mapToCartResponse(Cart cart) {
        CartResponse cartResponse = new CartResponse();
        cartResponse.setTimestamp(cart.getTimestamp().getTime());
        cartResponse.setItems(cart.getItems().stream().map(entry -> {
            var entryResponse = new CartResponse.CartEntryResponse();
            entryResponse.setProduct(productResponseConverter.mapToProductResponse(entry.getProduct()));
            entryResponse.setQuantity(entry.getQuantity());
            entryResponse.setPrice(entry.getProduct().getPrice().multiply(BigDecimal.valueOf(entry.getQuantity())));
            return entryResponse;
        }).collect(Collectors.toSet()));

        cartResponse.setTotalPrice(cartResponse.getItems().stream()
                .map(CartResponse.CartEntryResponse::getPrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO));
        return cartResponse;
    }
}
