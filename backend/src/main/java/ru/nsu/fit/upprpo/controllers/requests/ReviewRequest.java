package ru.nsu.fit.upprpo.controllers.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ReviewRequest {

    @NotBlank
    private long productId;

    @NotBlank
    private String text;
}
