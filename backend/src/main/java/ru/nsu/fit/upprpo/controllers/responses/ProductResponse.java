package ru.nsu.fit.upprpo.controllers.responses;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class ProductResponse {

    @Getter
    @Setter
    public static class CategoryWithoutChildren {
        long id;
        String name;

        public CategoryWithoutChildren(Category category) {
            id = category.getId();
            name = category.getName();
        }

    }

    private long id;
    private String name;
    private String description;
    private BigDecimal price;
    private double rating;
    private int ratingNumber;
    private String manufacturer;
    private List<String> imagesUrls;
    private List<CategoryWithoutChildren> categoryPath;

    public ProductResponse(Product product, int ratingNumber, double rating, List<Category> path) {
        id = product.getId();
        name = product.getName();
        description = product.getDescription();
        price = product.getPrice();
        this.ratingNumber = ratingNumber;
        this.rating = rating;
        manufacturer = product.getManufacturer();
        imagesUrls = new ArrayList<>();

        categoryPath = path.stream().map(CategoryWithoutChildren::new).collect(Collectors.toList());

    }

}
