package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Product;

import java.math.BigDecimal;
import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {
	Product findById(long id);
	List<Product> findByName(String name);
	List<Product> findAll();
	List<Product> findByCategory(Category category);

	@Query("select p from Product p " +
			"where ((?1 is null) or upper(p.name) like upper(?1)) " +
			"and ((?2 is null) or p.price >= ?2) " +
			"and ((?3 is null) or p.price <= ?3) " +
			"and ((?4 is null) or upper(p.manufacturer) like upper(?4))")
	List<Product> searchFull(@Nullable String name, @Nullable BigDecimal priceStart, @Nullable BigDecimal priceEnd, @Nullable String manufacturer);


}
