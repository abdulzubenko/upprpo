package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.Image;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.repositories.ImageRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;

import java.io.IOException;

@RestController
public class ImageController {
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ProductRepository productRepository;

    @PostMapping(Endpoints.UPLOAD_IMAGE)
    public ResponseEntity<Long> uploadImage(@RequestPart MultipartFile multipartImage,
                                            @RequestParam long productId) {

        Product product = productRepository.findById(productId);

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Image image = new Image();
        image.setProduct(product);
        try {
            image.setContent(multipartImage.getBytes());

            return ResponseEntity.ok(imageRepository.save(image).getId());
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping(path = Endpoints.IMAGE + "/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> downloadImage(@PathVariable Long imageId) {
        byte[] image = imageRepository.findById(imageId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                .getContent();

        return ResponseEntity.ok(new ByteArrayResource(image));
    }

}
