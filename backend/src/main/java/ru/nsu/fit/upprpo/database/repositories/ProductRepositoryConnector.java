package ru.nsu.fit.upprpo.database.repositories;

import ru.nsu.fit.upprpo.database.entities.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class ProductRepositoryConnector {

	private final ProductRepository productRepository;

	public ProductRepositoryConnector(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public List<Product> search(
			Optional<String> name,
			Optional<BigDecimal> minPrice,
			Optional<BigDecimal> maxPrice,
			Optional<String> manufacturer) {

		return productRepository.searchFull(
					name.orElse(null), minPrice.orElse(null), maxPrice.orElse(null), manufacturer.orElse(null));
	}


}
