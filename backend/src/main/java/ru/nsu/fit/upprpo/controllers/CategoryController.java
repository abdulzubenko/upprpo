package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;

import java.util.Set;

@RestController
@RequestMapping(value = "/", produces = "application/json")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping(Endpoints.CATEGORY)
    public String getCategories() {
        return new Gson().toJson(categoryRepository.findByName("root").map(Category::getChildren).orElse(Set.of()));
    }
}
