package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.requests.CategoryRequest;
import ru.nsu.fit.upprpo.controllers.requests.ProductRequest;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;

@RestController
public class AdminController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @PostMapping(path = Endpoints.CREATE_CATEGORY)
    public ResponseEntity<String> createCategory(@RequestBody CategoryRequest categoryRequest) {

        Category parent = categoryRepository.findByName(categoryRequest
                .getParent())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));

        if (categoryRepository.existsByName(categoryRequest.getName())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Category category = new Category();
        category.setName(categoryRequest.getName());
        categoryRepository.save(category);

        parent.getChildren().add(category);
        categoryRepository.save(parent);

        return ResponseEntity.ok("OK");
    }

    @PostMapping(path = Endpoints.CREATE_PRODUCT)
    public ResponseEntity<String> createProduct(@RequestBody ProductRequest productRequest) {

        Category category = categoryRepository.findById(productRequest
                        .getCategoryId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));


        Product product = new Product();
        product.setPrice(productRequest.getPrice());
        product.setManufacturer(productRequest.getManufacturer());
        product.setDescription(productRequest.getDescription());
        product.setName(productRequest.getName());
        product.setCategory(category);

        productRepository.save(product);

        return ResponseEntity.ok("OK");
    }

}
