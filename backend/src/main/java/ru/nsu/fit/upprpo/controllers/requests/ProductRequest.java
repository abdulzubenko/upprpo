package ru.nsu.fit.upprpo.controllers.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Getter
@Setter
public class ProductRequest {

    @NotBlank
    private String name;

    private long categoryId;

    private String manufacturer;

    private String description;

    private BigDecimal price;
}
