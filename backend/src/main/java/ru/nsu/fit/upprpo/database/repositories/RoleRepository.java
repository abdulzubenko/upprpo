package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.upprpo.database.entities.Role;
import ru.nsu.fit.upprpo.database.entities.RoleType;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRoleType(RoleType roleType);
}
