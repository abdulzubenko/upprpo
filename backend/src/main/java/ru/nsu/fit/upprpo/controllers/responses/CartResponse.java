package ru.nsu.fit.upprpo.controllers.responses;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
public class CartResponse {
    private Set<CartEntryResponse> items;
    private long timestamp;
    private BigDecimal totalPrice;

    @Getter
    @Setter
    public static class CartEntryResponse {
        private ProductResponse product;
        private int quantity;
        private BigDecimal price;
    }
}
