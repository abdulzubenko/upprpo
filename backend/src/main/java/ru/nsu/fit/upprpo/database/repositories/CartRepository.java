package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.Cart;

public interface CartRepository extends CrudRepository<Cart, Long> {
	Cart findById(long id);
}
