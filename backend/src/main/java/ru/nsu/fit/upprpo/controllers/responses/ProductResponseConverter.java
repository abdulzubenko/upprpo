package ru.nsu.fit.upprpo.controllers.responses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Image;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.entities.Rating;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ImageRepository;
import ru.nsu.fit.upprpo.database.repositories.RatingRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductResponseConverter {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private ImageRepository imageRepository;

    public ProductResponse mapToProductResponse(Product product) {
        if (product == null) {
            return null;
        }
        List<Category> path = new ArrayList<>();
        Collection<Category> categories = categoryRepository.findAll();
        List<Image> images = imageRepository.findByProduct(product);
        Category current = product.getCategory();
        while (current != null && !current.getName().equals("root")) {
            path.add(current);
            Category finalCurrent = current;
            current = categories.stream().filter(c -> c.getChildren().contains(finalCurrent)).findAny().orElse(null);
        }
        Collections.reverse(path);
        var ratings = ratingRepository.findByProduct(product);
        double averageRating = ratings.isEmpty() ? 0 : ratings.stream()
                .map(Rating::getValue)
                .reduce(0, Integer::sum) / (double) ratings.size();

        ProductResponse productResponse =  new ProductResponse(product, ratings.size(), averageRating, path);
        productResponse.setImagesUrls(images.stream()
                .map(image -> image.getId().toString())
                .map(name -> Endpoints.IMAGE + "/" + name)
                .collect(Collectors.toList()));
        return productResponse;
    }
}
