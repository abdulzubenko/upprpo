package ru.nsu.fit.upprpo.security.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.nsu.fit.upprpo.database.entities.Cart;
import ru.nsu.fit.upprpo.database.entities.Role;
import ru.nsu.fit.upprpo.database.entities.RoleType;
import ru.nsu.fit.upprpo.database.entities.User;
import ru.nsu.fit.upprpo.database.repositories.CartRepository;
import ru.nsu.fit.upprpo.database.repositories.RoleRepository;
import ru.nsu.fit.upprpo.database.repositories.UserRepository;

import java.util.HashSet;

@Service
@Component
public class UserService {

    @Autowired
    private UserRepository userEntityRepository;
    @Autowired
    private RoleRepository roleEntityRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CartRepository cartRepository;

    public User saveUser(User user) {
        User existingUser = findByLogin(user.getUsername());
        if (existingUser != null) {
            return existingUser;
        }

        Cart cart = new Cart();
        cart.setItems(new HashSet<>());
        cartRepository.save(cart);

        Role userRole = roleEntityRepository.findByRoleType(RoleType.ROLE_CUSTOMER).orElse(null);
        user.setRole(userRole);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setCart(cart);
        user.setHistory(new HashSet<>());
        return userEntityRepository.save(user);

    }

    public User findByLogin(String login) {
        return userEntityRepository.findByUsername(login)
                .orElse(null);
    }

    public User findByLoginAndPassword(String login, String password) {
        User userEntity = findByLogin(login);
        if (userEntity != null && passwordEncoder.matches(password, userEntity.getPassword())) {
            return userEntity;
        }
        return null;
    }

    public void deleteAll() {
        userEntityRepository.deleteAll();
    }
}