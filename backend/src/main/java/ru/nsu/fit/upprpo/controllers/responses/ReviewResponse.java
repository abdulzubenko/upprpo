package ru.nsu.fit.upprpo.controllers.responses;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.upprpo.database.entities.Review;

@Getter
@Setter
public class ReviewResponse {
    private String username;
    private String text;
    private long timestamp;
    private int rating; // 0 if no rating with this review

    public ReviewResponse(Review review, int rating) {
        username = review.getAuthor().getUsername();
        text = review.getText();
        timestamp = review.getTimestamp().getTime();
        this.rating = rating;
    }
}
