package ru.nsu.fit.upprpo.constants;

public class Endpoints {

    private Endpoints() {
    }

    public static final String PRODUCT = "/product";
    public static final String PRODUCTS = "/products";
    public static final String PRODUCTS_SEARCH = "/search";
    public static final String CATEGORY = "/categories";
    public static final String CART = "/cart";
    public static final String WISHLIST = "/wishlist";

    public static final String PURCHASE = "/purchase";
    public static final String HISTORY = "/purchases";
    public static final String CART_QUANTITY = "/cart/quantity";


    public static final String REGISTER = "/register";
    public static final String LOGIN = "/login";

    public static final String CREATE_CATEGORY = "/createcategory";
    public static final String CREATE_PRODUCT = "/createproduct";
    public static final String RATING = "/rating";
    public static final String REVIEW = "/review";
    public static final String REVIEWS_LIST = "/reviewslist";

    public static final String UPLOAD_IMAGE = "/uploadimage";
    public static final String IMAGE = "/img";

    public static final String FEED = "/feed";

}
