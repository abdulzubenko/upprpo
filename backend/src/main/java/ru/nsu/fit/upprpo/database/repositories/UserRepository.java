package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
	User findById(long id);
	boolean existsByUsername(String name);
	Optional<User> findByUsername(String name);
}
