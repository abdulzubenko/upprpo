FROM maven:3.8.3-jdk-11-slim AS build
COPY ./backend /home/app/build/backend
COPY ./pom.xml /home/app/build
RUN mvn -f /home/app/build clean package -P gcloud

FROM openjdk:11-jre

COPY --from=build /home/app/build/backend/target/backend-1.0-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-jar","-Dspring.profiles.active=prod","/app.jar"]
