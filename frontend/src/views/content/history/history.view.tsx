import { FC, useCallback, useEffect, useState } from 'react';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { backendUrl } from '../../../constants';
import { Cart, CartModel } from '../../../models/cart';
import { cartEntryComparator } from '../../../utils/cart.entry.comparator';
import { getCredentials } from '../../../utils/credentials.utils';
import CartItemView from './components/cart.item.view';

import styles from './styles/history.module.scss';


const cartComparator = (c1: Cart, c2: Cart) => c1.timestamp <= c2.timestamp ? 1 : 0;

const HistoryView: FC = () => {
    const [carts, setCarts] = useState<Cart[]>([]);

    const fetchCarts = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/purchases`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        })
            .then(response => response.json())
            .then((result: Cart[]) => {
                const cs = result.map(cart => new CartModel(cart)).sort(cartComparator);
                cs.forEach(c => c.cartEntries.sort(cartEntryComparator));
                setCarts(cs);
            });
    }, []);

    useEffect(() => {
        fetchCarts();
    }, [fetchCarts]);

    return (
        <div className={styles.root}>
            <div className={styles.carts}>
                {
                    carts.map((cart, idx) => <CartItemView key={idx} cart={cart} />)
                }
            </div>
        </div>
    );
};

export default HistoryView;
