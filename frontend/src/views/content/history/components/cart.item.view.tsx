import { FC } from 'react';

import { Cart } from '../../../../models/cart';
import { rnd, sum } from '../../../../utils/big.decimal.utils';
import ProductsItemView from '../../../components/products.item.view';

import styles from '../styles/cart.item.module.scss';


const CartItemView: FC<{
    cart: Cart;
}> = ({ cart }) => {
    return (
        <div className={styles.root}>
            <div className={styles.header}>
                <span>{new Date(cart.timestamp).toLocaleString()} - </span>
                <span>{cart.cartEntries.map(cartEntry => cartEntry.quantity).reduce(sum, '0')} products, </span>
                <span>{rnd(cart.totalPrice)} $</span>
            </div>
            <div className={styles.products}>
                {cart.cartEntries.map((cartEntry, idx) =>
                    <div className={styles.product} key={idx}>
                        <div className={styles.productItem}>
                            <ProductsItemView product={cartEntry.product} />
                        </div>
                        <div className={styles.details}>
                            <span>{cartEntry.quantity} products, </span>
                            <span>{rnd(cart.cartEntries[idx].price)} $</span>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};

export default CartItemView;
