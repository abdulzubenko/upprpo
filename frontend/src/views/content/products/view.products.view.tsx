import { FC, useCallback, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { backendUrl } from '../../../constants';
import { Product, ProductModel } from '../../../models/product';
import ProductsView from './components/products.view';


const ViewProductsView: FC<RouteComponentProps<{ categoryId: string }>> = ({ match }) => {
    const [products, setProducts] = useState<Product[]>([]);

    const fetchProducts = useCallback(() => {
        // todo get path from Route
        fetch(`${backendUrl}/products/${match.params.categoryId ?? ''}`, { method: 'GET' })
            .then(response => response.json())
            .then((result: Product[]) => setProducts(result.map(product => new ProductModel(product))));
    }, [match.params.categoryId]);

    useEffect(() => {
        fetchProducts();
    }, [fetchProducts, match.params.categoryId]);

    return <ProductsView products={products} />;
};

export default ViewProductsView;
