import { FC, useCallback, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { backendUrl } from '../../../constants';
import { Product, ProductModel } from '../../../models/product';
import ProductsView from './components/products.view';


const SearchProductsView: FC<RouteComponentProps<{ searchPhrase: string }>> = ({ match }) => {
    const [products, setProducts] = useState<Product[]>([]);

    const fetchProducts = useCallback(() => {
        fetch(`${backendUrl}/search?productName=%25${match.params.searchPhrase}%25`, { method: 'GET' })
            .then(response => response.json())
            .then((result: Product[]) => setProducts(result.map(product => new ProductModel(product))));
    }, [match.params.searchPhrase]);

    useEffect(() => {
        fetchProducts();
    }, [fetchProducts, match.params.searchPhrase]);

    return <ProductsView products={products} />;
};

export default SearchProductsView;
