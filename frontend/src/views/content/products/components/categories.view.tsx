import { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { backendUrl } from '../../../../constants';
import { Category, CategoryModel } from '../../../../models/category';

import styles from '../styles/categories.view.module.scss';


const categoryComparator = (c1: Category, c2: Category) => c1.name.localeCompare(c2.name);

const CategoryView: FC<{
    category: Category;
    isRoot?: boolean;
}> = ({ category, isRoot }) => {
    const className = isRoot
        ? styles.dropdown
        : [styles.dropdown, styles.dropdownContent].join(' ');

    return (
        <div className={className}>
            <Link to={'/products/' + category.id} className={styles.link}>
                {category.name}
            </Link>
            {category.children.map(childCategory =>
                <CategoryView key={childCategory.id} category={childCategory} />
            )}
        </div>
    );
};

const CategoriesView: FC = () => {
    const [categories, setCategories] = useState<Category[]>([]);

    const fetchCategories = () => {
        // todo get path from Route?
        fetch(`${backendUrl}/categories/`, { method: 'GET' })
            .then(response => response.json())
            .then((result: Category[]) => setCategories(result.map(category => new CategoryModel(category)).sort(categoryComparator)));
    };

    useEffect(() => {
        fetchCategories();
    }, []);

    return (
        <>
            {categories.map(category => <CategoryView key={category.id} category={category} isRoot={true} />)}
        </>
    );
};

export default CategoriesView;
