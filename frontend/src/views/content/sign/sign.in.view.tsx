import { FC, MouseEvent, useCallback, useRef } from 'react';
import { useHistory } from 'react-router-dom';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { backendUrl } from '../../../constants';
import { Credentials } from '../../../models/credentials';

import styles from './styles/sign.module.scss';


const SignInView: FC<{
    onSignedIn: (credetials: Credentials) => void;
}> = ({ onSignedIn }) => {
    const history = useHistory();

    const loginRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    const handleSubmit = useCallback((event: MouseEvent<HTMLElement>) => {
        event.preventDefault();

        const login = loginRef.current?.value;
        const password = passwordRef.current?.value;

        if (!login) {
            NotificationManager.warning('Login: empty field', 'Error');
            return;
        }

        if (!password) {
            NotificationManager.warning('Password: empty field', 'Error');
            return;
        }

        fetch(`${backendUrl}/login/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 'login': login, 'password': password })
        }).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                NotificationManager.error('Failed to sign in', response.status);
                throw new Error();
            }
        }).then(result => {
            onSignedIn({ login, token: result.token });
            history.push('/products');
        }).catch(_ => { });
    }, [history, loginRef, passwordRef, onSignedIn]);

    return (
        <div className={styles.root}>
            <h1>Sign In</h1>
            <form>
                <div>
                    <label>Login:</label>
                    <br />
                    <input ref={loginRef} type="text" placeholder="Login" />
                </div>
                <div>
                    <label>Password:</label>
                    <br />
                    <input ref={passwordRef} type="password" placeholder="Password" />
                </div>
                <button onClick={handleSubmit}>Sign In</button>
            </form>
        </div>
    );
};

export default SignInView;
