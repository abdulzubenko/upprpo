import { FC, MouseEvent, useCallback, useRef } from 'react';
import { useHistory } from 'react-router-dom';
// @ts-ignore
import { NotificationManager } from 'react-notifications';
import { backendUrl } from '../../../constants';

import styles from './styles/sign.module.scss';

const SignUpView: FC = () => {
    const history = useHistory();

    const loginRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);
    const repeatedPasswordRef = useRef<HTMLInputElement>(null);

    const handleSubmit = useCallback((event: MouseEvent<HTMLElement>) => {
        event.preventDefault();

        const login = loginRef.current?.value;
        const password = passwordRef.current?.value;
        const repeatedPassword = repeatedPasswordRef.current?.value;

        if (!login) {
            NotificationManager.warning('Login: empty field', 'Error');
            return;
        }

        if (!password) {
            NotificationManager.warning('Password: empty field', 'Error');
            return;
        }

        if (!repeatedPassword) {
            NotificationManager.warning('Repeate password: empty field', 'Error');
            return;
        }

        if (password !== repeatedPassword) {
            NotificationManager.warning('Passwords don\'t match', 'Error');
            return;
        }

        fetch(`${backendUrl}/register/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 'login': login, 'password': password })
        }).then(response => {
            if (response.ok) {
                history.push('/signin')
            } else {
                NotificationManager.error('Failed to sign up', response.status);
            }
        });
    }, [history, loginRef, passwordRef, repeatedPasswordRef]);

    return (
        <div className={styles.root}>
            <h1>Sign Up</h1>
            <form>
                <div>
                    <label>Login:</label>
                    <br />
                    <input ref={loginRef} type="text" placeholder="Login" />
                </div>
                <div>
                    <label>Password:</label>
                    <br />
                    <input ref={passwordRef} type="password" placeholder="Password" />
                </div>
                <div>
                    <label>Password:</label>
                    <br />
                    <input ref={repeatedPasswordRef} type="password" placeholder="Repeat password" />
                </div>
                <button onClick={handleSubmit}>Sign Up</button>
            </form>
        </div>
    );
};

export default SignUpView;
