import { ChangeEvent, FC, useCallback, useEffect, useState } from 'react';
// @ts-ignore
import { NotificationManager } from 'react-notifications';
import { useHistory } from 'react-router-dom';

import { backendUrl } from '../../../constants';
import { Cart, CartModel } from '../../../models/cart';
import { rnd, sum } from '../../../utils/big.decimal.utils';
import { getCredentials } from '../../../utils/credentials.utils';
import { cartEntryComparator } from '../../../utils/cart.entry.comparator';
import DeleteFromCartButton from '../../components/delete.from.cart.button';
import ProductsItemView from '../../components/products.item.view';

import styles from './styles/cart.module.scss';


const CartView: FC = () => {
    const [cart, setCart] = useState<Cart | null>(null);
    const history = useHistory();

    const fetchCartEntries = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/cart`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        })
            .then(response => response.json())
            .then((result: Cart) => {
                const c = new CartModel(result);
                c.cartEntries.sort(cartEntryComparator);
                setCart(c);
            });
    }, [setCart]);

    useEffect(() => {
        fetchCartEntries();
    }, [fetchCartEntries]);

    const handleQuantityChanged = useCallback((idx: number) => (event: ChangeEvent<HTMLInputElement>) => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        const productId = cart!.cartEntries[idx].product.id;
        const quantity = parseInt(event.target.value);

        fetch(`${backendUrl}/cart/quantity?productId=${productId}&quantity=${quantity}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        }).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(`${response.status}`);
            }
        }).then((result: Cart) => {
            const c = new CartModel(result);
            c.cartEntries.sort(cartEntryComparator);
            setCart(c);
        }).catch((e: Error) => NotificationManager.error('Failed to change quantity', e.message));
    }, [cart, setCart]);

    const handleDeletedFromCart = useCallback((productId: number) => {
        setCart(new CartModel({
            ...cart,
            items: cart!.cartEntries.filter(cartEntry => cartEntry.product.id !== productId)
        }));
    }, [cart, setCart]);

    const handleClear = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/cart/clear`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        }).then(response => {
            if (response.ok) {
                setCart(new CartModel({}));
            } else {
                NotificationManager.error('Failed to clear the cart', response.status);
            }
        });
    }, [setCart]);

    const handleBuy = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        if (!cart?.cartEntries.length) {
            NotificationManager.warning('Your cart is empty', 'Error');
            return;
        }

        fetch(`${backendUrl}/purchase`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        }).then(response => {
            if (response.ok) {
                NotificationManager.success('Don\'t stop', 'Congratulations');
                history.push('/history');
            } else {
                NotificationManager.error('Failed to purchase', response.status);
            }
        });
    }, [cart, history]);

    return cart
        ? (
            <div className={styles.root}>
                <div className={styles.products}>
                    {cart.cartEntries.map((cartEntry, idx) =>
                        <div className={styles.product} key={idx}>
                            <div className={styles.productItem}>
                                <ProductsItemView product={cartEntry.product} />
                            </div>
                            <div className={styles.actions}>
                                <input
                                    type="number"
                                    min="1"
                                    max="10"
                                    defaultValue={cartEntry.quantity}
                                    onChange={handleQuantityChanged(idx)} />
                                <div>{rnd(cart.cartEntries[idx].price)} $</div>
                                <DeleteFromCartButton product={cartEntry.product} onDeletedFromCart={handleDeletedFromCart} />
                            </div>
                        </div>
                    )}
                </div>
                <div className={styles.cart}>
                    <div>{cart.cartEntries.map(cartEntry => cartEntry.quantity).reduce(sum, '0')} products</div>
                    <div>{rnd(cart.totalPrice)} $</div>
                    <div onClick={handleClear}>Clear</div>
                    <div onClick={handleBuy}>Buy</div>
                </div>
            </div>
        ) : <></>;
};

export default CartView;
