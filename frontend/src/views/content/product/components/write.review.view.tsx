import { FC, useCallback, useRef } from 'react';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { backendUrl } from '../../../../constants';
import { Review, ReviewModel } from '../../../../models/review';
import { getCredentials } from '../../../../utils/credentials.utils';

import styles from '../styles/review.item.module.scss';


const WriteReviewView: FC<{
    productId: string;
    onReviewPosted: (review: Review) => void;
}> = ({ productId, onReviewPosted }) => {
    const credentials = getCredentials();
    const ratingRef = useRef<HTMLInputElement>(null);
    const textRef = useRef<HTMLInputElement>(null);

    const handleOnSubmit = useCallback(() => {
        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        if (!textRef.current?.value) {
            NotificationManager.warning('Review text field is empty', 'Error');
            return;
        }

        fetch(`${backendUrl}/rating?productId=${productId}&rating=${ratingRef.current?.value}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        });

        fetch(`${backendUrl}/review`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${credentials.token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                productId: productId,
                text: textRef.current?.value
            })
        }).then(response => {
            if (response.ok) {
                NotificationManager.success('Your review has been posted', 'Nice!');
                onReviewPosted(new ReviewModel({
                    username: credentials.login,
                    text: textRef.current?.value,
                    rating: ratingRef.current?.value,
                    timestamp: Date.now()
                }));
            } else {
                NotificationManager.error('Failed to post your review', response.status);
            }
        });
    }, [credentials, productId, onReviewPosted]);

    return credentials ? (
        <div>
            <input
                ref={ratingRef}
                type="number"
                min="1"
                max="5"
                defaultValue={5} />
            <input
                ref={textRef}
                type="text" />
            <span onClick={handleOnSubmit}>Submit</span>
        </div>
    ) : <></>;
};

export default WriteReviewView;
