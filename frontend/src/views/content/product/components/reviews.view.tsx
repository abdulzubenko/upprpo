import { FC, useCallback, useState } from 'react';
import { Review } from '../../../../models/review';
import ReviewItemView from './review.item.view';

import styles from '../styles/reviews.module.scss';
import WriteReviewView from './write.review.view';


const ReviewsView: FC<{
    productId: string;
    reviews: Review[];
    onReviewPosted: (review: Review) => void;
}> = ({ productId, reviews, onReviewPosted }) => {
    const [canWriteReview, setCanWriteReview] = useState<boolean>(true);

    const handleReviewPosted = useCallback((review: Review) => {
        onReviewPosted(review);
        setCanWriteReview(false);
    }, [onReviewPosted, setCanWriteReview]);

    return (
        <div className={styles.root}>
            {canWriteReview ? <WriteReviewView productId={productId} onReviewPosted={handleReviewPosted} /> : null}
            {reviews.map((review, idx) => <ReviewItemView key={idx} review={review} />)}
        </div>
    );
};

export default ReviewsView;
