import { FC } from 'react';
import { Link } from 'react-router-dom';
import { Product } from '../../../../models/product';
import { rnd } from '../../../../utils/big.decimal.utils';
import AddToCartButton from '../../../components/add.to.cart.button';
import AddToWishlistButton from '../../../components/add.to.wishlist.button';

import styles from '../styles/product.info.module.scss';


const ProductInfoView: FC<{
    product: Product;
}> = ({ product }) => {
    return (
        <>
            <div className={styles.categoriesWrapper}>
                {product.categoryPath.map((category, idx) => (
                    <span key={idx}>
                        <Link to={`/products/${category.id}`}>{category.name}</Link>
                        {
                            idx < product.categoryPath.length - 1
                                ? <span> &gt; </span>
                                : null
                        }
                    </span>
                ))}
            </div>
            <h1 className={styles.name}>{product.name}</h1>
            <div className={styles.mainInfoWrapper}>
                <div className={styles.imageWrapper}>
                    <img src={product.imagesUrls[0]} alt={`${product.manufacturer}: ${product.name}`} />
                </div>
                <div className={styles.descriptionWrapper}>
                    <div className={styles.priceAndRatingWrapper}>
                        <div>{`${rnd(product.price)} $`}</div>
                        <div>{`${product.rating} (${product.ratingNumber})`}</div>
                    </div>
                    <div>
                        <AddToWishlistButton product={product} />
                        <AddToCartButton product={product} />
                    </div>
                    <p className={styles.description}>
                        {product.description}
                    </p>
                </div>
            </div>
        </>
    );
};

export default ProductInfoView;
