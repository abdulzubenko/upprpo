import { FC } from 'react';
import { Review } from '../../../../models/review';

import styles from '../styles/review.item.module.scss';


const ReviewItemView: FC<{
    review: Review;
}> = ({ review }) => {
    return (
        <div>
            <div>{review.username}, {new Date(review.timestamp).toLocaleString()}</div>
            <div>{review.rating} / 5</div>
            <p>{review.text}</p>
        </div>
    );
};

export default ReviewItemView;
