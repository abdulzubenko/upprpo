import { FC, useCallback, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { backendUrl } from '../../../constants';
import { Product, ProductModel } from '../../../models/product';
import { Review, ReviewModel } from '../../../models/review';
import { getCredentials } from '../../../utils/credentials.utils';
import ProductInfoView from './components/product.info.view';
import ReviewsView from './components/reviews.view';

import styles from './styles/product.module.scss';


const reviewComparator = (r1: Review, r2: Review) => r1.timestamp <= r2.timestamp ? 1 : 0;

const ProductView: FC<RouteComponentProps<{ id: string }>> = ({ match }) => {
    const login = getCredentials()?.login;
    const [product, setProduct] = useState<Product>();
    const [reviews, setReviews] = useState<Review[]>([]);

    const fetchProduct = useCallback(() => {
        fetch(`${backendUrl}/product/${match.params.id}`, { method: 'GET' })
            .then(response => response.json())
            .then(result => setProduct(new ProductModel(result)));
    }, [setProduct, match.params.id]);

    const fetchReviews = useCallback(() => {
        fetch(`${backendUrl}/reviewslist?productId=${match.params.id}`, { method: 'GET' })
            .then(response => response.json())
            .then((result: Review[]) => setReviews(result.map(review => new ReviewModel(review)).sort(reviewComparator)));
    }, [setReviews, match.params.id]);

    const handleReviewPosted = useCallback((review: Review) => {
        setReviews([review, ...reviews.filter(review => review.username !== login)]);
    }, [reviews, setReviews, login]);

    useEffect(() => {
        fetchProduct();
        fetchReviews();
    }, [fetchProduct, fetchReviews, match.params.id]);

    if (!product) {
        return <></>;
    }

    return (
        <>
            <div className={styles.productInfo}>
                <ProductInfoView product={product} />
            </div>
            <div className={styles.reviews}>
                <ReviewsView productId={match.params.id} reviews={reviews} onReviewPosted={handleReviewPosted} />
            </div>
        </>
    );
};

export default ProductView;
