import { FC, useCallback, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { getCredentials } from '../../../utils/credentials.utils';
import { backendUrl } from '../../../constants';
import { Product, ProductModel } from '../../../models/product';
import ProductsItemView from '../../components/products.item.view';
import DeleteFromWishlistButton from '../../components/delete.from.wishlist.button';

import styles from './styles/wishlist.module.scss';


const WishlistView: FC<RouteComponentProps<{ categoryId: string }>> = ({ match }) => {
    const [products, setProducts] = useState<Product[]>([]);

    const fetchProducts = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/wishlist`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        })
            .then(response => response.json())
            .then((result: Product[]) => setProducts(result.map(product => new ProductModel(product))));
    }, []);

    useEffect(() => {
        fetchProducts();
    }, [fetchProducts, match.params.categoryId]);

    const handleDeletedFromWishlist = useCallback((productId: number) => {
        setProducts(products.filter(product => product.id !== productId));
    }, [products, setProducts]);

    return (
        <div className={styles.root}>
            <h1>Wishlist</h1>
            <div className={styles.products}>
                {products.map((product, idx) =>
                    <div className={styles.product} key={idx}>
                        <div className={styles.productItem}>
                            <ProductsItemView product={product} />
                        </div>
                        <div className={styles.deleteProductItem}>
                            <DeleteFromWishlistButton product={product} onDeletedFromWishlist={handleDeletedFromWishlist} />
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};

export default WishlistView;
