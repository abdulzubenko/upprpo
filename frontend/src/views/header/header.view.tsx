import { FC, MouseEvent, useCallback, useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

import styles from './styles/header.module.scss';


const HeaderView: FC<{
    login: string | null;
    onSignedOut: () => void;
}> = ({ login, onSignedOut }) => {
    const history = useHistory();
    const searchRef = useRef<HTMLInputElement>(null);

    const handleSearch = useCallback(() => {
        if (searchRef.current?.value) {
            history.push(`/search/${searchRef.current?.value}`);
        }
    }, [history]);

    const handleSignedOut = useCallback((event: MouseEvent<HTMLElement>) => {
        event.preventDefault();

        if (window.confirm('Are you sure you want to sign out?')) {
            onSignedOut();
        }
    }, [onSignedOut]);

    const publicSection =
        <>
            <Link className={styles.logo} to={'/products'}></Link>
            <Link className={styles.link} to={'/products'}>Upprpo-Shop</Link>
            <div className={styles.searchbar}>
                <input ref={searchRef} type="search" className={styles.searchInput} />
                <div className={styles.searchIcon} onClick={handleSearch}></div>
            </div>
        </>;

    const userSection = login !== null
        ? (
            <>
                <Link className={styles.link} to={'/history'}>History</Link>
                <Link className={styles.link} to={'/wishlist'}>Wishlist</Link>
                <Link className={styles.link} to={'/cart'}>Cart</Link>
            </>
        ) : null;

    const signSection = login === null
        ? (
            <span>
                <Link className={styles.link} to={'/signin'}>Sign in</Link>
                <span> / </span>
                <Link className={styles.link} to={'/signup'}>Sign up</Link>
            </span>
        ) : (
            <>
                <a href='' onClick={handleSignedOut}>{`Sign out (${login})`}</a>
            </>
        );

    return (
        <div className={styles.root}>
            {publicSection}
            {userSection}
            {signSection}
        </div>
    );
};

export default HeaderView;
