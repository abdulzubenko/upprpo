import { FC } from 'react';

import styles from './styles/footer.module.scss';

const FooterView: FC = () => {
    return (
        <div className={styles.root}>
            <span>© 2021 Upprpo. You better buy something</span>
            <a href="https://gitlab.com/omarkelov/upprpo" target="blank" rel="noreferrer">Gitlab</a>
        </div>
    );
};

export default FooterView;
