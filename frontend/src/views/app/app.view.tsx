import { FC, useCallback, useState } from 'react';
// @ts-ignore
import { NotificationContainer } from 'react-notifications';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import { Credentials } from '../../models/credentials';
import { deleteCredentials, getCredentials, saveCredentials } from '../../utils/credentials.utils';
import FooterView from '../footer/footer.view';
import HeaderView from '../header/header.view';
import ProductView from '../content/product/product.view';
import ViewProductsView from '../content/products/view.products.view';
import SearchProductsView from '../content/products/search.products.view';
import SignUpView from '../content/sign/sign.up.view';
import SignInView from '../content/sign/sign.in.view';
import WishlistView from '../content/wishlist/wishlist.view';
import CartView from '../content/cart/cart.view';
import HistoryView from '../content/history/history.view';

import styles from './styles/app.module.scss';
import 'react-notifications/lib/notifications.css';


const ContentWrapper: FC = (props: React.PropsWithChildren<{}>) => {
    return (
        <div className={styles.contentWrapper}>
            {props.children}
        </div>
    );
};

const App = () => {
    const [credentials, setCredentials] = useState(getCredentials());

    const handleSignedIn = useCallback((credentials: Credentials) => {
        saveCredentials(credentials);
        setCredentials(credentials);
    }, []);

    const handleSignedOut = useCallback(() => {
        deleteCredentials();
        setCredentials(null);
    }, []);

    const handleMainRedirect = useCallback(() => <Redirect to='/products'/>, []);

    return (
        <div className={styles.root}>
            <BrowserRouter>
                <header className={styles.header}>
                    <ContentWrapper>
                        <HeaderView login={credentials ? credentials.login : null} onSignedOut={handleSignedOut} />
                    </ContentWrapper>
                </header>
                <main className={styles.content}>
                    <ContentWrapper>
                        <Switch>
                            <Route exact path='/' render={handleMainRedirect} />
                            <Route path='/products/:categoryId?' component={ViewProductsView} />
                            <Route path='/search/:searchPhrase' component={SearchProductsView} />
                            <Route path='/product/:id' component={ProductView} />
                            <Route path='/signup' component={SignUpView} />
                            <Route path='/signin' component={() => <SignInView onSignedIn={handleSignedIn} />} />
                            <Route path='/wishlist' component={WishlistView} />
                            <Route path='/cart' component={CartView} />
                            <Route path='/history' component={HistoryView} />
                        </Switch>
                    </ContentWrapper>
                </main>
                <footer className={styles.footer}>
                    <ContentWrapper>
                        <FooterView />
                    </ContentWrapper>
                </footer>
            </BrowserRouter>
            <NotificationContainer />
        </div>
    );
};

export default App;
