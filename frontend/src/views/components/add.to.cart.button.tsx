import { FC, useCallback } from 'react';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { backendUrl } from '../../constants';
import { Product } from '../../models/product';
import { getCredentials } from '../../utils/credentials.utils';

import styles from './styles/buttons.module.scss';

const AddToCartButton: FC<{
    product: Product;
}> = ({ product }) => {
    const handleAddedToCart = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/cart?productId=${product.id}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        }).then(response => {
            if (response.ok) {
                NotificationManager.success(`The product "${product.name}" has been added to your cart`, response.status);
            } else {
                NotificationManager.error('Failed to add the product to your cart', response.status);
            }
        });
    }, [product.id, product.name]);

    return (
        <span onClick={handleAddedToCart} className={styles.actionButton}>Add to cart</span>
    );
};

export default AddToCartButton;
