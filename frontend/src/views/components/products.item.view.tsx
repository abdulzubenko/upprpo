import { FC } from 'react';
import { Link } from 'react-router-dom';

import { Product } from '../../models/product';
import { rnd } from '../../utils/big.decimal.utils';
import AddToCartButton from './add.to.cart.button';
import AddToWishlistButton from './add.to.wishlist.button';

import styles from './styles/products.item.module.scss';


const ProductsItemView: FC<{
    product: Product;
}> = ({ product }) => {
    return (
        <div className={styles.root}>
            <div className={styles.image}>
                <img src={product.imagesUrls[0]} alt={`${product.manufacturer}: ${product.name}`} />
            </div>
            <div className={styles.productInfo}>
                <Link to={`/product/${product.id}`}>
                    {product.name}
                </Link>
                <div>{`${rnd(product.price)} $`}</div>
                <div>
                    <div>{`${product.rating} (${product.ratingNumber})`}</div>
                    <div>
                        <AddToWishlistButton product={product} />
                        <AddToCartButton product={product} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductsItemView;
