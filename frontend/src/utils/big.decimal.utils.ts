import bigDecimal from "js-big-decimal";


export const rnd = (n: number | string) => bigDecimal.round(n, 2);
export const sum = (n1: number | string, n2: number | string) => bigDecimal.add(n1, n2);
