import { CartEntry } from "../models/cart.entry";


export const cartEntryComparator = (c1: CartEntry, c2: CartEntry) => c1.product.name.localeCompare(c2.product.name);
