import { CartEntry, CartEntryModel } from "./cart.entry";

export interface Cart {
    cartEntries: CartEntry[];
    totalPrice: number;
    timestamp: number;
}

export class CartModel implements Cart {
    cartEntries = [];
    totalPrice = 0;
    timestamp = 0;

    constructor(o: any) {
        this.cartEntries = o?.items?.map ? o.items.map((cartEntry: any) => new CartEntryModel(cartEntry)) : this.cartEntries;
        this.totalPrice = o.totalPrice ?? this.totalPrice;
        this.timestamp = o.timestamp ?? this.timestamp;
    }
}
