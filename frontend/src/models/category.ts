export interface Category {
    id: number;
    name: string;
    children: Category[];
}

export class CategoryModel implements Category {
    id = 0;
    name = '';
    children = [];

    constructor(o: any) {
        this.id = o.id ?? this.id;
        this.name = o.name ?? this.name;
        this.children = o.children ?? this.children;
    }
}
