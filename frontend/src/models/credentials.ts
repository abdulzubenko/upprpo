export interface Credentials {
    login: string;
    token: string;
}
