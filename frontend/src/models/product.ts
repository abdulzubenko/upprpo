import { backendUrl } from "../constants";
import { Category, CategoryModel } from "./category";

export interface Product {
    id: number;
    name: string;
    description: string;
    price: number;
    rating: number;
    ratingNumber: number;
    manufacturer: string;
    imagesUrls: string[];
    categoryPath: Omit<Category, 'children'>[];
};

export class ProductModel implements Product {
    id = 1;
    name = 'Super Guitar';
    description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
    price = 555;
    rating = 4.5;
    ratingNumber = 32;
    manufacturer = 'Anybody';
    imagesUrls = ['https://attrade.ru/upload/iblock/552/fp_Q_header.jpg'];
    categoryPath = [];

    constructor(o: any) {
        this.id = o.id ?? this.id;
        this.name = o.name ?? this.name;
        this.description = o.description ?? this.description;
        this.price = o.price ?? this.price;
        this.rating = o.rating ?? this.rating;
        this.ratingNumber = o.ratingNumber ?? this.ratingNumber;
        this.manufacturer = o.manufacturer ?? this.manufacturer;
        this.imagesUrls = o?.imagesUrls?.map ? o.imagesUrls.map((imageUrl: string) => backendUrl + imageUrl) : this.imagesUrls;
        this.categoryPath = o.categoryPath?.map((category: any) => new CategoryModel(category)) ?? this.categoryPath;
    }
}
