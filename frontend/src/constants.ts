export const backendUrl = process?.env?.NODE_ENV === 'development'
    ? 'http://localhost:8081'
    : 'https://intrepid-stage-336111.nw.r.appspot.com';
